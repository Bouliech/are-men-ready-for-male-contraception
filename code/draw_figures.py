#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Production de figures à partir de l'article suivant :

Amouroux, M., Mieusset, R., Desbriere, R., Opinel, P., Karsenty, G., Paci, M., ... & Perrin, J. (2018).
Are men ready to use thermal male contraception? Acceptability in two French
populations: New fathers and new providers.
PloS one, 13(5), e0195824.
https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0195824

@author=GARCON
@authorwebsite=www.garcon.link
@creationdate=7 Nov. 2021


Copyright (C) 2022 GARCON

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
Place, Suite 330, Boston, MA 02111-1307 USA
"""

import os.path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns


fmtImages = ".svg"
# Images will be saved under this format (suffix of plt.savefig)

figureDir = "../figures/code-raw-output/"
# Images will be saved in this directory (prefix of plt.savefig)

storeImages = True
# If True, figures are saved in files but not shown
# If False, figures are not saved in files but always shown




# DONNEES
#=========

# Table 2 : La contraception et vous
#------------------------------------

N_nf = 305

ctrcp_methods = np.array([
    "Pilule",
    "DIU",
    "Implant",
    "Rythmes",
    "Pres. fem.",
    "Coupe cervicale",
    "Pres. masc.",
    "Retrait",
    "CM horm.",
    "CM therm.",
    "Aucune",
    "Autre",
])
rep_nf_befbb = np.array([154, 29, 6, 0, 1, 0, 91, 40, 0, 0, 51, 17])
rep_nf_aftbb = np.array([110, 73, 10, 1, 3, 0, 60, 12, 1, 1, 2, 21])

who_chose = {"Lui": 10, "Elle": 87, "Les deux": 203}




# Table 3 : La contraception masculine
#--------------------------------------

N_nf = 304
N_mnp = 97
N_fnp = 203

mctrcp_methods = np.array(
    ["Preservatif", "Retrait", "Vasectomie", "CM horm.", "CM therm.", "Aucune", "Autre"]
)
rep_nf_known = np.array([298, 193, 146, 34, 8, 7, 4])
rep_mnp_known = np.array([96, 63, 83, 31, 25, 0, 4])
rep_fnp_known = np.array([202, 148, 181, 43, 20, 1, 6])

rep_nf_pref = np.array([196, 49, 8, 5, 30, 18, 6])
rep_mnp_pref = np.array([71, 3, 3, 2, 12, 5, 1])
rep_fnp_pref = np.array([130, 10, 4, 16, 27, 15, 1])

would_you_use_cm = {"nf": 178, "mnp": 68, "fnp": 150}
effectifs = {"nf": N_nf, "mnp": N_mnp, "fnp": N_fnp}
key2name = {"nf": "Nvx pères", "mnp": "Nvx médecins H", "fnp": "Nvx médecins F"}

why_yes = {
    "Partage responsabilité": 91,
    "Mesure suppl. pour éviter grossesse": 27,
    "Ne pas avoir d'enfant sans le savoir": 14,
    "Éviter effets 2ndaires contr. fem.": 67,
    "Ne pas avoir d'enfant avec une autre": 4,
    "Autre": 1,
}

why_no = {
    "Peu pratique": 27,
    "Effet 2ndaires": 18,
    "Contr. = pour femmes": 16,
    "Atteinte virilité": 9,
    "Pas du tout intéressé": 49,
    "Autre": 23,
}

# Table 4 et 6 : Connaissance et acceptibilite CMT
#--------------------------------------------------

ever_heard_tmc = {"Oui": 17, "Non": 305 - 17}

N_nf = 303
advantages_tmc = {
    "Écologique": 87,
    "Peu cher": 61,
    "Pas d'effet secondaire": 117,
    "Efficacité": 60,
    "Non-hormonal": 110,
    "Naturel": 158,
    "Réversible": 109,
    "Autre": 27,
}
advantages_tmc_names = np.array([
    "Écologique",
    "Peu cher",
    "Pas d'effet secondaire",
    "Efficacité",
    "Non-hormonal",
    "Naturel",
    "Réversible",
    "Autre",
    "Aucun",
])
advantages_tmc_nf = np.array([87,61,117,60,110,158,109,27,0])
advantages_tmc_mnp_forpatients = np.array([41,59,43, 11,70,54,59,1,6])
advantages_tmc_mnp_forthem = np.array([43,48,36,17,65,52,60,0,6])
advantages_tmc_fnp_forpatients = np.array([102,124,113,27,150,145,133,3,3])
advantages_tmc_fnp_forthem = np.array([109,99,108,29,131,124,136,1,6])

disadvantages_tmc = {
    "Efficacité retardée": 94,
    "Réversibilité retardée": 64,
    "Temps de port requis": 170,
    "Inesthétique": 67,
    "Inconfortable": 118,
    "Doit être porté sans échec": 131,
    "Perte de confiance": 103,
    "Atteinte à la virilité": 41,
    "Risque d'IST": 51,
    "Autre": 27,
}

disadvantages_tmc_names = np.array([
    "Efficacité retardée",
    "Réversibilité retardée",
    "Temps de port requis",
    "Inesthétique",
    "Inconfortable",
    "Doit être porté sans échec",
    "Perte de confiance",
    "Atteinte à la virilité",
    "Risque d'IST",
    "Autre",
])
disadvantages_tmc_nf = np.array([94,64,170,67,118,131,103,41,51,27])
disadvantages_tmc_mnp_forpatients = np.array([57,37,75,44,52,65,51,29,0,5])
disadvantages_tmc_mnp_forthem = np.array([48,39,75,35,64,65,50,17,29,2])
disadvantages_tmc_fnp_forpatients = np.array([130,100,159,119,91,150,112,68,95,0])
disadvantages_tmc_fnp_forthem = np.array([115,88,151,93,88,120,124,44,6,6])

would_you_try = {
    "nettement oui": 22,
    "plutôt oui": 67,
    "plutôt non": 92,
    "nettement non": 123,
}

would_you_try_labels = [
    "nettement oui",
    "plutôt oui",
    "plutôt non",
    "nettement non",
]
would_you_try_nf = [22,67,92,123]
would_you_try_mnp_forthem = [5,25,38,29]
would_you_try_fnp_forthem = [23,68,83,29]

best_period_life = {
    "Célibataire": 12,
    "En relation instable": 8,
    "Avant 1er enfant": 17,
    "Entre deux enfants": 40,
    "Après une naissance": 64,
    "Quand CF impossible": 59,
    "Sans opinion": 99,
}


# Table 5 : Nvx medecins comme prescipteurs de CM
#-------------------------------------------------

N_mnp = 97
N_fnp = 203

asked_for_ctrcp_labels = np.array(["Souvent", "Rarement", "Jamais"])
asked_for_ctrcp_mnp = np.array([40, 43, 14])
asked_for_ctrcp_fnp = np.array([116, 61, 26])

feel_unequiped_mnp = 43
feel_unequiped_fnp = 110

offer_MC_labels = np.array(["Souvent", "Rarement", "Jamais"])
offer_MC_mnp = np.array([16, 44, 37])
offer_MC_fnp = np.array([36, 103, 64])


offered_MC_noncondom_labels = np.array(["Souvent", "Rarement", "Jamais"])
offered_MC_noncondom_mnp = np.array([6,40,51])
offered_MC_noncondom_fnp = np.array([3,95,105])

interested_in_course_mnp = 74
interested_in_course_fnp = 173

what_MC_noncondom_labels = np.array(["Vasectomie", "CM horm.", "CM therm.", "Aucune", "Autre"])
what_MC_noncondom_mnp = np.array([37,5,3,55,2])
what_MC_noncondom_fnp = np.array([78,9,3,118,5])


# GENERATION DES FIGURES
#========================
sns.set()

sns.set_palette("Set1")

# Table 2 : La contraception et vous
#------------------------------------
N_nf = 305


# [Bar] Moyen de contraception avant/après grossesse
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(ctrcp_methods))
sdex = np.argsort(rep_nf_befbb)[::-1]
width = 0.3
plt.figure()
plt.bar(
    x - width / 2,
    rep_nf_befbb[sdex] * 100 / rep_nf_befbb.sum(),
    tick_label=ctrcp_methods[sdex],
    width=width,
    label="Avant grossesse",
)
plt.bar(
    x + width / 2,
    rep_nf_aftbb[sdex] * 100 / rep_nf_aftbb.sum(),
    width=width,
    label="Après grossesse",
)
plt.title("Moyen de contraception principal")
plt.xticks(rotation=90)
plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "moyen_contraception_principal" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Pie] Qui choisit la contraception
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

plt.figure()
plt.pie(
    who_chose.values(),
    labels=who_chose.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
)
plt.title("Qui choisit la contraception ?")
if storeImages:
    figname = os.path.join(figureDir, "qui_choisit" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)




# Table 3 : La contraception masculine
#--------------------------------------

N_nf = 304
N_mnp = 97
N_fnp = 203

# [Bar] Contraception masculine connues
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_palette("tab10")

x = np.arange(len(mctrcp_methods))
sdex = np.argsort(rep_nf_known)[::-1]
width = 0.3
plt.figure()
plt.bar(
    x - 2 * width / 3,
    rep_nf_known[sdex] * 100 / N_nf,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx pères",
)
plt.bar(
    x,
    rep_mnp_known[sdex] * 100 / N_mnp,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx médecins H",
)
plt.bar(
    x + 2 * width / 3,
    rep_fnp_known[sdex] * 100 / N_fnp,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx médecins F",
)
plt.title("Contraceptions masculines connues")
plt.xticks(rotation=90)
plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "CM_connues" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Bar] Contraception masculine préférées
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(mctrcp_methods))
sdex = np.argsort(rep_nf_pref)[::-1]
width = 0.3
plt.figure()
plt.bar(
    x - 2 * width / 3,
    rep_nf_pref[sdex] * 100 / N_nf,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx pères",
)
plt.bar(
    x,
    rep_mnp_pref[sdex] * 100 / N_mnp,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx médecins H",
)
plt.bar(
    x + 2 * width / 3,
    rep_fnp_pref[sdex] * 100 / N_fnp,
    tick_label=mctrcp_methods[sdex],
    width=2 * width / 3,
    label="Nvx médecins F",
)
plt.title("Contraception masculine préférée")
plt.xticks(rotation=90)
plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "CM_preferee" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)



# [Pie] Accepteriez-vous d'utiliser une contraception masculine ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
cml = list(plt.get_cmap("tab20").colors)[::-1]

for pop in key2name.keys():
    plt.figure()
    plt.pie(
        [would_you_use_cm[pop], effectifs[pop] - would_you_use_cm[pop]],
        labels=["Oui", "Non"],
        startangle=90,
        counterclock=False,
        autopct="%1.1f%%",
        colors = [cml.pop(),cml.pop()]
    )
    plt.title("Accepteriez-vous d'utiliser une contraception masculine ?")
    if storeImages:
        figname = os.path.join(figureDir, pop + "_accepte_utiliser_CM" + fmtImages)
        plt.savefig(figname)
        plt.close() 
        print("Figure saved:", figname)
    else:
        plt.show(block=False)
    
# [Bar] Accepteriez-vous d'utiliser une contraception masculine ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(key2name))
plt.figure()
plt.bar(
    x, [100*would_you_use_cm[pop]/effectifs[pop] for pop in key2name.keys()],
    tick_label=[key2name[pop] for pop in key2name.keys()],
)
plt.title("Accepteriez-vous d'utiliser une contraception masculine ?")
if storeImages:
    figname = os.path.join(figureDir, "accepte_utiliser_CM" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Bar] Raisons d'accepter
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(why_yes))
val = np.array(list(why_yes.values()))
lab = np.array(list(why_yes.keys()))
sdex = np.argsort(val)
plt.figure()
plt.barh(x, val[sdex], tick_label=lab[sdex])
plt.title("Raisons pour accepter")
ax = plt.gca()
ax.invert_xaxis()
if storeImages:
    figname = os.path.join(figureDir, pop + "raison_accepter" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)

# [Bar] Raisons de refuser
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(why_no))
val = np.array(list(why_no.values()))
lab = np.array(list(why_no.keys()))
sdex = np.argsort(val)
plt.figure()
plt.barh(x, val[sdex], tick_label=lab[sdex])
plt.title("Raisons pour refuser")
if storeImages:
    figname = os.path.join(figureDir, pop + "raison_refuser" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)





# Table 4 : Connaissance et acceptibilite CMT
#---------------------------------------------

N_nf = 303

sns.set_palette("Set1")

# [Pie] Connaissez-vous la CMT
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

plt.figure()
plt.pie(
    ever_heard_tmc.values(),
    labels=ever_heard_tmc.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
)
plt.title("Avez-vous déjà entendu parler de la CMT ?")
if storeImages:
    figname = os.path.join(figureDir, "deja_entendu_parler_CMT" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)

# [Pie] Avantages CMT
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

plt.figure()
plt.pie(
    advantages_tmc.values(),
    labels=advantages_tmc.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
)
plt.title("Avantages de la CMT")
if storeImages:
    figname = os.path.join(figureDir, "avantages_CMT" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Pie] Inconvénients CMT
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

plt.figure()
plt.pie(
    disadvantages_tmc.values(),
    labels=disadvantages_tmc.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
)
plt.title("Inconvénients de la CMT")
if storeImages:
    figname = os.path.join(figureDir, "inconvenients_CMT" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Pie] Accepteriez-vous d'utiliser la CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

colors = [cm.coolwarm(x) for x in np.linspace(0.1, 0.9, len(would_you_try))]
plt.figure()
plt.pie(
    would_you_try.values(),
    labels=would_you_try.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
    colors = colors,
)
plt.title("Accepteriez-vous d'utiliser la CMT ?")
if storeImages:
    figname = os.path.join(figureDir, "accepte_utiliser_CMT" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Pie] Meilleure période de la vie pour CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

plt.figure()
plt.pie(
    best_period_life.values(),
    labels=best_period_life.keys(),
    startangle=90,
    counterclock=False,
    autopct="%1.1f%%",
)
plt.title("Quelle est la meilleure période de la vie d'un homme pour utiliser la CMT ?")
if storeImages:
    figname = os.path.join(figureDir, "meilleure_periode" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)



# [Bar] Avantages CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_style("darkgrid", {"grid.color": "black", 'grid.linestyle': ':'})

x = np.arange(len(advantages_tmc_names))
sdex = np.argsort(advantages_tmc_nf)[::-1]
width = 0.6
plt.figure()
plt.bar(
    x - 2 * width / 3,
    advantages_tmc_nf[sdex] * 100 / N_nf,
    tick_label=advantages_tmc_names[sdex],
    width=2 * width / 3,
    # label="Nvx pères",
)
plt.ylim([0,65])
plt.ylabel("% des NP")
plt.title("Avantages de la CMT")
plt.xticks(rotation=90)
# plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "bar_avantages_cmt_nf" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Bar] Inconvénients CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

x = np.arange(len(disadvantages_tmc_names))
sdex = np.argsort(disadvantages_tmc_nf)[::-1]
width = 0.6
plt.figure()
plt.bar(
    x - 2 * width / 3,
    disadvantages_tmc_nf[sdex] * 100 / N_nf,
    tick_label=disadvantages_tmc_names[sdex],
    width=2 * width / 3,
    # label="Nvx pères",
)
plt.ylim([0,65])
plt.ylabel("% des NP")
plt.title("Avantages de la CMT")
plt.xticks(rotation=90)
# plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "bar_inconvenients_cmt_nf" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Bar] Avantages CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_style("darkgrid", {"grid.color": "black", 'grid.linestyle': ':'})
sns.set_palette("tab10")
c_nf = cm.tab20.colors[0]
c_mnp_forthem = cm.tab20.colors[4]
c_mnp_forpatients = cm.tab20.colors[5]
c_fnp_forthem = cm.tab20.colors[6]
c_fnp_forpatients = cm.tab20.colors[7]

x = np.arange(len(advantages_tmc_names))
sdex = np.argsort(advantages_tmc_nf)[::-1]
width = 0.3
plt.figure()
plt.bar(
    x - 2 * width / 3,
    advantages_tmc_nf[sdex] * 100 / N_nf,
    tick_label=advantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx pères",
    color=[c_nf],
)
plt.bar(
    x - width / 3,
    advantages_tmc_mnp_forthem[sdex] * 100 / N_mnp,
    tick_label=advantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins H util.",
    color=[c_mnp_forthem],
)
plt.bar(
    x,
    advantages_tmc_mnp_forpatients[sdex] * 100 / N_mnp,
    tick_label=advantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins H presc.",
    color=[c_mnp_forpatients],
)
plt.bar(
    x + width/3,
    advantages_tmc_fnp_forthem[sdex] * 100 / N_fnp,
    tick_label=advantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins F util.",
    color=[c_fnp_forthem],
)
plt.bar(
    x + 2 * width / 3,
    advantages_tmc_fnp_forpatients[sdex] * 100 / N_fnp,
    tick_label=advantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins F presc.",
    color=[c_fnp_forpatients],
)
plt.ylim([0,80])
plt.ylabel("% des NP")
plt.title("Avantages de la CMT")
plt.xticks(rotation=90)
plt.legend(loc="best")
if storeImages:
    figname = os.path.join(figureDir, "bar_avantages_cmt_all" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)

print("\nRangs avantages pour NMH")
sdex = np.argsort(advantages_tmc_mnp_forthem)[::-1]
print("  pour eux:",advantages_tmc_names[sdex])
sdex = np.argsort(advantages_tmc_mnp_forpatients)[::-1]
print("  pour leur patients:",advantages_tmc_names[sdex])


print("\nRangs avantages pour NMF")
sdex = np.argsort(advantages_tmc_fnp_forthem)[::-1]
print("  pour elles:",advantages_tmc_names[sdex])
sdex = np.argsort(advantages_tmc_fnp_forpatients)[::-1]
print("  pour leur patients:",advantages_tmc_names[sdex])


rk_adv_mnp_forthem = np.argsort(advantages_tmc_mnp_forthem)
rk_adv_mnp_forpatients = np.argsort(advantages_tmc_mnp_forpatients)
rk_adv_fnp_forthem = np.argsort(advantages_tmc_fnp_forthem)[::-1]
rk_adv_fnp_forpatients = np.argsort(advantages_tmc_fnp_forpatients)[::-1]


# [Bar] Avantages CMT (changement de rangs)
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_palette("deep")
sns.set_style("darkgrid")

x = np.arange(len(advantages_tmc_names))
width = 0.3
plt.figure()
plt.bar(
    x - width / 2,
    rk_adv_mnp_forthem - rk_adv_mnp_forpatients,
    tick_label=advantages_tmc_names,
    width=width,
    label="Nvx médecins H",
)
plt.bar(
    x + width / 2,
    rk_adv_fnp_forthem - rk_adv_fnp_forpatients,
    tick_label=advantages_tmc_names,
    width=width,
    label = "Nvx médecins F",
)
plt.title("Changement de rang presc./util. des avantages")
plt.xticks(rotation=90)
plt.legend(loc="best")
if storeImages:
    figname = os.path.join(figureDir, "rank_change_advantages" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)


# [Bar] Inconvénients CMT ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_style("darkgrid", {"grid.color": "black", 'grid.linestyle': ':'})

x = np.arange(len(disadvantages_tmc_names))
sdex = np.argsort(disadvantages_tmc_nf)[::-1]
width = 0.3
plt.figure()
plt.bar(
    x - 2 * width / 3,
    disadvantages_tmc_nf[sdex] * 100 / N_nf,
    tick_label=disadvantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx pères",
    color=[c_nf],
)
plt.bar(
    x - width / 3,
    disadvantages_tmc_mnp_forthem[sdex] * 100 / N_mnp,
    tick_label=disadvantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins H util.",
    color=[c_mnp_forthem],
)
plt.bar(
    x,
    disadvantages_tmc_mnp_forpatients[sdex] * 100 / N_mnp,
    tick_label=disadvantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins H presc.",
    color=[c_mnp_forpatients],
)
plt.bar(
    x + width/3,
    disadvantages_tmc_fnp_forthem[sdex] * 100 / N_fnp,
    tick_label=disadvantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins F util.",
    color=[c_fnp_forthem],
)
plt.bar(
    x + 2 * width / 3,
    disadvantages_tmc_fnp_forpatients[sdex] * 100 / N_fnp,
    tick_label=disadvantages_tmc_names[sdex],
    width= width / 3,
    label="Nvx médecins F presc.",
    color=[c_fnp_forpatients],
)
plt.ylim([0,80])
plt.ylabel("% des NP")
plt.title("Inconvénients de la CMT")
plt.xticks(rotation=90)
plt.legend(loc="best")
if storeImages:
    figname = os.path.join(figureDir, "bar_inconvenients_cmt_all" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)





# Table 5 : Nvx médecins precripteurs de CM
#-------------------------------------------



# [Bar] Avez-vous déjà proposé de la CM (hors préservatif) ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_palette("deep")

x = np.arange(len(offered_MC_noncondom_labels))
sdex = np.argsort(offered_MC_noncondom_mnp+offered_MC_noncondom_fnp)[::-1]
width = 0.3
plt.figure()
plt.bar(
    # x - width / 2,
    x,
    offered_MC_noncondom_mnp[sdex] * 100 / N_mnp,
    tick_label=offered_MC_noncondom_labels[sdex],
    width= width / 2,
    label="Nvx médecins H",
)
plt.bar(
    x + width / 2,
    offered_MC_noncondom_fnp[sdex] * 100 / N_fnp,
    tick_label=offered_MC_noncondom_labels[sdex],
    width= width / 2,
    label="Nvx médecins H",
)
plt.ylim([0,65])
plt.ylabel("% des NP")
plt.title("Avez-vous déjà proposé de la CM (hors préservatif) ?")
plt.xticks(rotation=90)
plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "offered_MC_noncondom" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)

# [Bar] Quelle CM proposée (hors préservatif) ?
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sns.set_palette("Set1")

x = np.arange(len(what_MC_noncondom_labels))
sdex = np.argsort(what_MC_noncondom_mnp+what_MC_noncondom_fnp)[::-1]
width = 0.3
plt.figure()
plt.bar(
    # x - width / 2,
    x,
    what_MC_noncondom_mnp[sdex] * 100 / N_mnp,
    tick_label=what_MC_noncondom_labels[sdex],
    width= width / 2,
    label="Nvx médecins H",
)
plt.bar(
    x + width / 2,
    what_MC_noncondom_fnp[sdex] * 100 / N_fnp,
    tick_label=what_MC_noncondom_labels[sdex],
    width= width / 2,
    label="Nvx médecins H",
)
plt.ylim([0,65])
plt.ylabel("% des NP")
plt.title("Quelle CM avez-vous déjà proposé (hors préservatif) ?")
plt.xticks(rotation=90)
plt.legend(loc="best")
# ax = plt.gca()
# ax.set_xticklabels(labels=ctrcp_methods,rotation=30)
if storeImages:
    figname = os.path.join(figureDir, "what_MC_noncondom" + fmtImages)
    plt.savefig(figname)
    plt.close()
    print("Figure saved:", figname)
else:
    plt.show(block=False)
